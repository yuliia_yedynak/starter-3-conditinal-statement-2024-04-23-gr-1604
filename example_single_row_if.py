debugging = False


def foo():
    return 42


if debugging: print("About to call function foo()")
print(foo())
